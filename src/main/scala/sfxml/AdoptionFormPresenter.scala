package sfxml

import com.jfoenix.controls.JFXButton
import scalafx.event.ActionEvent
import scalafx.scene.control.{ChoiceBox, TextArea, TextField}
import scalafx.scene.layout.GridPane
import scalafxml.core.macros.sfxml

@sfxml
class AdoptionFormPresenter(private val sizeTextField: TextField,
                            private val breedTextField: TextField,
                            private val sexChoiceBox: ChoiceBox[String],
                            private val additionalInfoTextArea: TextArea,
                            private val grid: GridPane,
                            private val button: JFXButton) {

  def handleSubmit(event: ActionEvent) {
    grid.gridLinesVisible() = !grid.gridLinesVisible()
  }

  def handleClear(event: ActionEvent) {
    sizeTextField.text = ""
    breedTextField.text = ""
    sexChoiceBox.selectionModel().clearSelection()
    additionalInfoTextArea.text = ""
  }

  grid.onMouseClicked = e => println("dioporco")
  sexChoiceBox.onMouseClicked = e => println("diporco")


}
